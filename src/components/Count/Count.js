import React from 'react';

const Count = props => {
    return (
        <div>Tries: {props.count}</div>
    );
};

export default Count;