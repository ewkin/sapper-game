import React from 'react';
import './Element.css'
const Element = props => {
    let elementClasses = ['element'];

    if(props.show){
        elementClasses.push('element-Show')
    }

    return (
        <div className={elementClasses.join(' ')} onClick={props.openElement}>
            {props.ring}
        </div>
    );
};

export default Element;