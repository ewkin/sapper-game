import React from 'react';
import Element from "../Element/Element";

const Elements = ({elements, openElement}) => {
    return elements.map(element => (
                <Element
                    key = {element.id}
                    ring = {element.ring}
                    show = {element.show}
                    openElement = {() => openElement(element.id, element.ring, element.show, element.found)}
                />
           ))
};

export default Elements;