import React, {useState} from "react";
import './App.css';
import Elements from "../../components/Elements/Elements";
import ResetButton from "../../components/ResetButton/ResetButton";
import Count from "../../components/Count/Count";

const App = () =>{
  const [id, setId] = useState(1);
  const [count, setCount] = useState(0);
  const createElements = () =>{
        let random = Math.floor(Math.random() * (36 - 1 + 1)) + 1;
        let idCopy = id;
        const elementsCopy = [];
        for(let i=1; i<=36; i++){
            if(i === random){
                let element = {show: false, ring: 'O', id: idCopy, found:false}
                elementsCopy.push(element);
            } else {
                let element = {show: false, ring: null, id: idCopy, found:false}
                elementsCopy.push(element);
            }
            idCopy++
            setId(idCopy);
        }
        return elementsCopy
    };
  const [elements, setElements] =useState(()=>createElements());

  const openElement = (id, ring, show, found) =>{
      if(!show){
          if(!found){
              let countCopy = count;
              countCopy++
              setCount(countCopy);
              const index = elements.findIndex(element => element.id ===id);
              const elementCopy = {...elements[index]};
              elementCopy.show = true;
              const elementsCopy = [...elements];
              elementsCopy[index] = elementCopy;
              setElements(elementsCopy);
              if(ring === 'O'){
                  elementCopy.show = true;
                  setTimeout(()=>foundRing(index), 100);
              }
          }
      }
  };

  const foundRing = index =>{
      const elementsCopy = [...elements];
      for(let i =0; i<elementsCopy.length; i++){
          const elementCopy = {...elements[i]};
          elementCopy.found = true;
          if(i===index){
          elementCopy.show = true;
          }
          elementsCopy[i] = elementCopy;
      }
      setElements(elementsCopy);
      alert('You won');
  }


  const reset = () => {
      let countCopy = count;
      countCopy=0;
      setCount(countCopy);
      const elementsCopyNew = createElements();
      const elementsCopy = [...elements];
      for(let i = 0;i<elementsCopy.length;i++){
          elementsCopy[i]=elementsCopyNew[i];
      }
      setElements(elementsCopy);
  }


  return (
      <div className="App">
          <div className='Elements'>
            <Elements elements={elements} openElement={openElement}/>
          </div>
          <Count count={count}/>
          <ResetButton reset={reset}/>
      </div>
  );
};

export default App;